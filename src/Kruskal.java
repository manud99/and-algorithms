import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class Kruskal {
    public int[][] find(int[][] edges, int n) {
        ArrayList<int[]> spanningTree = new ArrayList<>(n - 1);
        UnionFind unionFind = new UnionFind(n);

        // Sort edges ascending by their weight
        Arrays.sort(edges, Comparator.comparingInt(edge -> edge[2]));

        for (int[] edge : edges) {
            // Ignore if both edges belong already to the same tree.
            if (unionFind.find(edge[0]) == unionFind.find(edge[1])) continue;

            // Add edge to spanning tree
            spanningTree.add(edge);
            unionFind.union(edge[0], edge[1]);

            // If spanning tree is complete, then return our result
            if (spanningTree.size() == n - 1) break;
        }

        return spanningTree.toArray(new int[spanningTree.size()][]);
    }
}
