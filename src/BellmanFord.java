import java.util.Arrays;

public class BellmanFord {

    public int[] find(int[][][] edges, int start) {
        int[] d = new int[edges.length];
        Arrays.fill(d, Integer.MAX_VALUE);
        d[start] = 0;

        // Go through all vertices (|V| - 1) times
        for (int i = 0; i < edges.length - 1; i++) {
            // For each vertex
            for (int m = 0; m < edges.length; m++) {
                // For each edge of the vertex
                for (int n = 0; n < edges[m].length; n++) {
                    int to = edges[m][n][0];
                    // Update distance if it's shorter than the old one
                    if (d[m] + edges[m][n][1] < d[to]) {
                        d[to] = d[m] + edges[m][n][1];
                    }
                }
            }
        }

        // If we decrease a distance in the |V|-th iteration, then our graph has a negative cycle
        for (int m = 0; m < edges.length; m++) {
            for (int n = 0; n < edges[m].length; n++) {
                int to = edges[m][n][0];
                if (d[m] + edges[m][n][1] < d[to]) {
                    throw new RuntimeException("Negative cycle detected");
                }
            }
        }

        return d;
    }
}
