import java.util.ArrayDeque;
import java.util.Arrays;

public class BFSShortestPath {

    public int find(int[][][] adjList, int start, int end) {
        ArrayDeque<Integer> queue = new ArrayDeque<>();
        int[] d = new int[adjList.length];
        Arrays.fill(d, -1);

        queue.add(start);
        d[start] = 0;

        while (!queue.isEmpty()) {
            int el = queue.poll();

            for (int[] adj : adjList[el]) {
                // We just ignore the edge's weight
                if (d[adj[0]] != -1) continue;

                queue.add(adj[0]);
                // Store the depth of the current node
                d[adj[0]] = d[el] + 1;
            }
        }


        return d[end];
    }
}
