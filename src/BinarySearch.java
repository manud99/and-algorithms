
public class BinarySearch {

    int[] arr;

    public BinarySearch(int[] arr) {
        this.arr = arr;
    }

    public int search(int num) {
        int left = 0;
        int right = arr.length - 1;

        while (left < right) {
            int mid = (left + right) / 2;

            if (arr[mid] == num)
                return mid;
            else if (num < arr[mid])
                right = mid - 1;
            else
                left = mid + 1;
        }

        return -1;
    }

}
