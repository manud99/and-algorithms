import java.util.ArrayList;

public class Node {
    int key;
    ArrayList<Node> adjList = new ArrayList<>();

    public Node() {
    }

    public Node(int key) {
        this.key = key;
    }
}
