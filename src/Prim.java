import java.util.ArrayList;
import java.util.HashSet;

public class Prim {
    public int[][] find(int[][][] adjList, int n, int startNode) {
        ArrayList<int[]> spanningTree = new ArrayList<>(n - 1);
        HashSet<Integer> components = new HashSet<>(n - 1);

        components.add(startNode);

        // While our spanningTree isn't complete
        while (spanningTree.size() < n - 1) {
            int[] minEdge = null;

            for (int component : components) {
                for (int[] edge : adjList[component]) {
                    // If the other vertex isn't already in our components set and the edge's weight is smaller than our current minimum
                    if (!components.contains(edge[1]) && (minEdge == null || edge[2] < minEdge[2])) {
                        minEdge = edge;
                    }
                }
            }

            // If there is no minEdge, something is wrong
            if (minEdge == null) throw new RuntimeException("Graph is disconnected");

            // Add minEdge to our spanning tree
            spanningTree.add(new int[]{Math.min(minEdge[0], minEdge[1]), Math.max(minEdge[0], minEdge[1]), minEdge[2]});
            components.add(minEdge[1]);
        }

        return spanningTree.toArray(new int[spanningTree.size()][]);
    }
}
