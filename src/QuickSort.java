
public class QuickSort {

    public void sort(int[] array) {
        sort(array, 0, array.length - 1);
    }

    /**
     * @param array our array to sort
     * @param start inclusive left bound
     * @param end inclusive right bound
     */
    private void sort(int[] array, int start, int end) {
        if (start >= end)
            return;

        // Partition our array into two half. Left is the position of our selected pivot.
        int left = fastPartition(array, start, end);
//        int left = easyPartition(array, start, end);

        // Sort recursively from our start to one to the left of our pivot ...
        sort(array, start, left - 1);
        // ... and from our pivot until our end.
        sort(array, left, end);
    }

    /**
     * Partition method which is much simpler to remember
     */
    private int easyPartition(int[] array, int start, int end) {
        // The last element is our pivot
        int pivot = array[end];
        int left = start;

        for (int i = start; i < end; i++) {
            // Swap all elements that are smaller than our pivot to the left of our `left` pointer.
            if (array[i] < pivot) {
                swap(array, left++, i);
            }
        }

        // Now move our pivot right in between both half
        swap(array, left, end);

        return left;
    }

    /**
     * Partition method which generally is faster.
     */
    private int fastPartition(int[] arr, int left, int right) {
        int mid = (left + right) >>> 1; // equal to: (left + right) / 2
        int pivot = arr[mid];

        while (left <= right) {
            while (arr[left] < pivot)
                ++left;
            while (arr[right] > pivot)
                --right;

            if (left <= right) {
                swap(arr, left, right);
                ++left;
                --right;
            }
        }

        return left;
    }

    private void swap(int[] arr, int i, int j) {
        if (i == j)
            return;

        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}
