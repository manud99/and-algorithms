import java.util.Arrays;

public class FloydWarshall {
    public int[][] solve(int[][] adjMatrix, int n) {
        // Initialize distances table. Theoretically, we could just copy the adjMatrix.
        int[][] distances = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    // Set all diagonal elements to 0
                    distances[i][j] = 0;

                } else if (adjMatrix[i][j] != Integer.MAX_VALUE) {
                    // There is an edge between `i` and `j`, so copy its weight
                    distances[i][j] = adjMatrix[i][j];

                } else {
                    // Set entry to infinity
                    distances[i][j] = Integer.MAX_VALUE;
                }
            }
        }

        for (int i = 0; i < n; i++) {
            // Create table for all paths which include the node `i`
            int[][] nextDimension = new int[n][];
            for (int j = 0; j < distances.length; j++) {
                nextDimension[j] = Arrays.copyOf(distances[j], n);
            }

            for (int u = 0; u < n; u++) {
                for (int v = 0; v < n; v++) {
                    // Continue if a path over `i` still equals to infinity
                    if (distances[u][i] == Integer.MAX_VALUE || distances[i][v] == Integer.MAX_VALUE) continue;

                    // Either the current path or the path via the node `i` is shorter
                    nextDimension[u][v] = Math.min(distances[u][v], distances[u][i] + distances[i][v]);
                }
            }

            distances = nextDimension;
        }

        return distances;
    }
}
