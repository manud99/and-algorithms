import java.util.Arrays;
import java.util.PriorityQueue;

public class Dijkstra {

    // Inner class to compare edges
    private static class Pair implements Comparable<Pair> {
        int index;
        int distance;

        public Pair(int index, int distance) {
            this.index = index;
            this.distance = distance;
        }

        @Override
        public int compareTo(Pair o) {
            return Integer.compare(this.distance, o.distance);
        }
    }

    public int[] find(int[][][] edges, int start) {
        PriorityQueue<Pair> priorityQueue = new PriorityQueue<>();
        int[] distances = new int[edges.length];
        int[] parents = new int[edges.length];

        // Per default all distances are infinite and no node has a parent.
        Arrays.fill(distances, Integer.MAX_VALUE);
        Arrays.fill(parents, -1);

        // Only exception: our start node
        priorityQueue.add(new Pair(start, 0));
        distances[start] = 0;

        while (!priorityQueue.isEmpty()) {
            // Get the element with the shortest distance from the priority queue
            Pair pair = priorityQueue.poll();
            int from = pair.index;
            int distance = pair.distance;

            // For each neighbour of our element
            for (int i = 0; i < edges[from].length; i++) {
                int to = edges[from][i][0];
                int newDistance = distance + edges[from][i][1];

                // If the new distance is shorter than the old one.
                if (newDistance >= distances[to]) continue;

                // Update the shortest distance for the current element
                // We do not decrease or remove the old pair from the priority queue, instead we just add a new pair.
                // This is better performance-wise and does not change the correctness of the algorithm
                priorityQueue.add(new Pair(to, newDistance));
                distances[to] = newDistance;
                parents[to] = from;
            }
        }

        return distances;
    }
}
