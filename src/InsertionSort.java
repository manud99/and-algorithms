
public class InsertionSort {

    public void sort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            // Get index of the position where our element should be
            int pos = binarySearch(arr, i - 1, arr[i]);

            int tmp = arr[i];

            // Move all elements between `pos` and `i - 1` one position to the right
            for (int j = i; j > pos; j--) {
                arr[j] = arr[j - 1];
            }

            // Move our element to it's correct position
            arr[pos] = tmp;
        }
    }

    public int binarySearch(int[] arr, int end, int num) {
        int left = 0;
        int right = end;

        if (num > arr[end])
            return end + 1;

        while (left < right) {
            int mid = (left + right) / 2;

            if (arr[mid] == num)
                return mid;
            else if (num < arr[mid])
                if (mid > 0 && num > arr[mid - 1])
                    return mid;
                else
                    right = mid - 1;
            else if (mid < end && num < arr[mid + 1])
                return mid + 1;
            else
                left = mid + 1;
        }

        return 0;

    }
}
