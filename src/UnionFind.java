class UnionFind {
    int[] parents;
    int[] rank;

    UnionFind(int n) {
        // Parents of all nodes. Self-linking nodes are roots of their individual trees
        this.parents = new int[n];
        // Here we store the heights of all trees
        this.rank = new int[n];

        for (int i = 0; i < this.parents.length; i++) {
            this.parents[i] = i;
            this.rank[i] = 1;
        }
    }

    public int find(int x) {
        // Go up the tree of x until we are at a root of our initial node
        while (x != parents[x]) {
            x = parents[x];
        }

        return x;
    }

    public void union(int u, int v) {
        // Get the roots of our given elements
        int parentU = this.find(u);
        int parentV = this.find(v);

        if (rank[parentU] > rank[parentV]) {
            // If the tree (whose nodes can have more than 2 children) of `u` is higher than the one of `v`,
            // then we append the root of `v` directly under the root of `u`
            parents[parentV] = parentU;

        } else if (rank[parentU] < rank[parentV]) {
            // Vice versa ...
            parents[parentU] = parentV;

        } else {
            // If both trees have equal length we choose one of them,
            // then increment its size and append the second tree under the root of the first one
            parents[parentU] = parentV;
            rank[parentU]++;
        }
    }
}
