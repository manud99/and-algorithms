public class MergeSort {

    public void sort(int[] arr) {
        sort(arr, 0, arr.length);
    }

    /**
     * @param arr our array to sort
     * @param left inclusive left bound
     * @param right exclusive right bound
     */
    private void sort(int[] arr, int left, int right) {
        // If there is only one element left
        if (left + 1 >= right) {
            return;
        }

        int mid = (left + right) / 2;

        // Recursively sort all subarrays
        sort(arr, left, mid);
        sort(arr, mid, right);

        int l = left;
        int r = mid;
        int k = left;

        // Create a copy of our array, so that we don't accidentally overwrite the array's values
        int[] copy = new int[arr.length];

        // Merge values
        while (l < mid && r < right) {
            if (arr[l] < arr[r]) {
                copy[k++] = arr[l++];
            } else {
                copy[k++] = arr[r++];
            }
        }

        // One array still has remaining values, that we now need to copy
        while (l < mid) {
            copy[k++] = arr[l++];
        }
        while (r < right) {
            copy[k++] = arr[r++];
        }

        // Write values from our copy to the original array
        for (int i = left; i < right; i++) {
            arr[i] = copy[i];
        }
    }
}
