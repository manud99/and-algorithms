
public class HeapSort {

    public void sort(int[] arr) {
        // Initialize our heap condition
        for (int i = arr.length / 2; i >= 0; i--) {
            restoreMaxHeap(arr, i, arr.length);
        }

        // Sort the array by using the heap condition
        for (int i = arr.length - 1; i >= 1; i--) {
            swap(arr, 0, i);
            restoreMaxHeap(arr, 0, i);
        }
    }

    private void restoreMaxHeap(int[] arr, int i, int n) {
        // While the next index is still in the array's bounds
        while (2 * i + 1 < n) {
            int j = 2 * i + 1;

            // If right tree node is bigger than the left one
            if (j + 1 < n && arr[j + 1] > arr[j])
                j = j + 1;

            // If the parent is bigger than both children then we are finished for this element
            if (arr[i] >= arr[j])
                return;

            // Just move the bigger element to the parent's position
            swap(arr, i, j);

            // Continue next with the child's index
            i = j;
        }
    }

    private void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}
