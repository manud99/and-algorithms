import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class TopologicalSorting {
    public Node[] find(ArrayList<Node> graph) {
        Set<Node> marked = new HashSet<>();
        ArrayList<Node> postOrder = new ArrayList<>(graph.size());

        for (Node u : graph) {
            if (marked.contains(u)) continue;

            visit(u, marked, postOrder);
        }

        Collections.reverse(postOrder);
        Node[] result = new Node[postOrder.size()];

        return postOrder.toArray(result);
    }

    private void visit(Node u, Set<Node> marked, ArrayList<Node> postOrder) {
        marked.add(u);

        for (Node v : u.adjList) {
            if (marked.contains(v)) continue;

            visit(v, marked, postOrder);
        }

        postOrder.add(u);
    }
}
