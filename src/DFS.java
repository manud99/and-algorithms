import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

public class DFS {

    public Set<Node> search(Node startNode) {
        // Create a set that we can use throughout our search
        Set<Node> marked = new HashSet<>();
        search(startNode, marked);

        return marked;
    }

    private void search(Node node, Set<Node> marked) {
        // Now mark that we have processed this node
        marked.add(node);

        // For each adjacent vertex of our node
        for (Node v : node.adjList) {
            // If we already processed it, then continue
            if (marked.contains(v)) continue;

            // Search recursively through our graph
            search(v, marked);
        }
    }

    public Set<Node> searchIterative(Node startNode) {
        // We use a stack (first in, last out) to iterate through are graph
        // BFS in contrast uses a queue (first in, first out)
        Deque<Node> stack = new ArrayDeque<>();
        stack.addFirst(startNode);

        // Create a set that we can use throughout our search
        Set<Node> marked = new HashSet<>();
        marked.add(startNode);

        while (!stack.isEmpty()) {
            Node node = stack.removeFirst();

            for (Node v : node.adjList) {
                // If we already processed it, then continue
                if (marked.contains(v)) continue;

                // Add node to stack and mark it as processed
                stack.addFirst(v);
                marked.add(v);
            }
        }

        return marked;
    }
}
