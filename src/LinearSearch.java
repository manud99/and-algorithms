
public class LinearSearch {

    int[] arr;

    public LinearSearch(int[] arr) {
        this.arr = arr;
    }

    public int search(int num) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == num)
                return i;
        }
        return -1;
    }
}
