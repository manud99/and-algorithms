import java.util.ArrayList;
import java.util.Arrays;

public class Johnson {
    /**
     * WARNING: this is a very slow implementation of Johnson's algorithm!
     */
    public int[][] solve(int[][] adjMatrix, int n) {
        // First create vertex the new node that is connected to all nodes with weight 0
        int[][] modifiedAdjMatrix = new int[n + 1][];
        for (int i = 0; i < adjMatrix.length; i++) {
            modifiedAdjMatrix[i] = Arrays.copyOf(adjMatrix[i], n + 1);
        }
        modifiedAdjMatrix[n] = new int[n + 1];

        // Transform adjMatrix to an edges array for the Bellman-Ford algorithm
        int[][][] modifiedEdges = adjMatrixToEdgesArray(modifiedAdjMatrix, n + 1);

        // Run Bellman-Ford from the new vertex to get the shortest path to all nodes
        int[] heights = new BellmanFord().find(modifiedEdges, n);

        // Modify all edge weights to a positive number with the results from Bellman-Ford
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                // Skip if there is no edge
                if (adjMatrix[i][j] == Integer.MAX_VALUE) continue;

                // Calculate positive weight by formula
                adjMatrix[i][j] += heights[i] - heights[j];
            }
        }

        // Transform adjMatrix to an edges array for the Dijkstra algorithm
        int[][][] edges = adjMatrixToEdgesArray(adjMatrix, n);

        // Run Dijkstra n times and create final distance table from the results
        int[][] matrix = new int[n][n];
        for (int i = 0; i < n; i++) {
            // Run Dijkstra from node `i`
            int[] distances = new Dijkstra().find(edges, i);

            // Copy all values to the final matrix
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    matrix[i][j] = 0;
                } else {
                    matrix[i][j] = distances[j] + heights[j] - heights[i];
                }
            }
        }

        return matrix;
    }

    private int[][][] adjMatrixToEdgesArray(int[][] adjMatrix, int n) {
        int[][][] edges = new int[n][][];
        for (int i = 0; i < n; i++) {
            ArrayList<int[]> list = new ArrayList<>();

            for (int j = 0; j < n; j++) {
                // Skip if there is no edge
                if (adjMatrix[i][j] == Integer.MAX_VALUE) continue;

                list.add(new int[]{j, adjMatrix[i][j]});
            }

            edges[i] = list.toArray(new int[list.size()][]);
        }
        return edges;
    }
}
