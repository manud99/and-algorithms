import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class BFS {
    public Set<Node> search(Node startNode) {
        LinkedList<Node> queue = new LinkedList<>();
        Set<Node> marked = new HashSet<>();

        queue.add(startNode);

        while (!queue.isEmpty()) {
            // Get first element in our queue
            Node u = queue.poll();

            // For each adjacent vertex `v`
            for (Node v : u.adjList) {
                // Continue if we already checked this vertex `v`
                if (marked.contains(v)) continue;

                // Add `v` to our queue and mark it as processed
                queue.add(v);
                marked.add(v);
            }
        }

        return marked;
    }
}
