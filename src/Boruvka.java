import java.util.ArrayList;

public class Boruvka {
    public int[][] find(int[][][] adjList, int n) {
        ArrayList<int[]> spanningTree = new ArrayList<>(n - 1);
        UnionFind unionFind = new UnionFind(n);

        // While our spanningTree isn't complete
        while (spanningTree.size() < n - 1) {
            int[][] minEdges = new int[n][];

            // for each vertex in our adjList
            for (int j = 0; j < adjList.length; j++) {
                int parent = unionFind.find(j);

                for (int[] edge : adjList[j]) {
                    // If the adjacent vertices are not in the same union and the edge's weight is smaller than the current minimum
                    if (unionFind.find(edge[0]) != unionFind.find(edge[1]) && (minEdges[parent] == null || edge[2] < minEdges[parent][2])) {
                        minEdges[parent] = edge;
                    }
                }
            }

            for (int[] min : minEdges) {
                // If adjacent vertices still are in different unions, then add the edge to our spanning tree
                if (min == null || unionFind.find(min[0]) == unionFind.find(min[1])) continue;

                spanningTree.add(new int[]{Math.min(min[0], min[1]), Math.max(min[0], min[1]), min[2]});
                unionFind.union(min[0], min[1]);
            }
        }

        return spanningTree.toArray(new int[spanningTree.size()][]);
    }
}
