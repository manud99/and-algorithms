import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SpanningTreeTests {

    /**
     * Structure of each element: {0: fromNode, 1: toNode, 2: weight}
     */
    public int[][] edges;

    /**
     * <img src="./doc-files/graph-spanning-tree.svg">
     */
    @Before
    public void setup() {
        this.edges = new int[][]{{0, 1, 5}, {0, 2, 2}, {1, 3, 3}, {2, 3, 7}, {3, 4, 9}, {3, 4, 11}, {4, 6, 1}, {5, 6, 6}};
    }

    @Test
    public void testBoruvka() {
        Boruvka boruvka = new Boruvka();
        int[][] spanningTree = boruvka.find(toAdjList(edges, 7), 7);

        int[][] expected = {{0, 1, 5}, {0, 2, 2}, {1, 3, 3}, {3, 4, 9}, {4, 6, 1}, {5, 6, 6}};
        assertArrayIsSimilar(expected, spanningTree);
    }

    @Test
    public void testKruskal() {
        Kruskal kruskal = new Kruskal();
        int[][] spanningTree = kruskal.find(edges, 7);

        int[][] expected = {{0, 1, 5}, {0, 2, 2}, {1, 3, 3}, {3, 4, 9}, {4, 6, 1}, {5, 6, 6}};
        assertArrayIsSimilar(expected, spanningTree);
    }

    @Test
    public void testPrim() {
        Prim prim = new Prim();
        int[][] spanningTree = prim.find(toAdjList(edges, 7), 7, 0);

        int[][] expected = {{0, 1, 5}, {0, 2, 2}, {1, 3, 3}, {3, 4, 9}, {4, 6, 1}, {5, 6, 6}};
        assertArrayIsSimilar(expected, spanningTree);
    }

    private int[][][] toAdjList(int[][] edges, int n) {
        ArrayList<ArrayList<Integer[]>> list = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            list.add(i, new ArrayList<>());
        }

        for (int[] edge : edges) {
            ArrayList<Integer[]> u = list.get(edge[0]);
            ArrayList<Integer[]> v = list.get(edge[1]);

            u.add(new Integer[]{edge[0], edge[1], edge[2]});
            v.add(new Integer[]{edge[1], edge[0], edge[2]});

            list.set(edge[0], u);
            list.set(edge[1], v);
        }

        int[][][] adjList = new int[list.size()][][];

        for (int i = 0; i < list.size(); i++) {
            int[][] row = new int[list.get(i).size()][];

            for (int j = 0; j < list.get(i).size(); j++) {
                int[] el = new int[]{list.get(i).get(j)[0], list.get(i).get(j)[1], list.get(i).get(j)[2]};
                row[j] = el;
            }

            adjList[i] = row;
        }

        return adjList;
    }

    private void assertArrayIsSimilar(int[][] expected, int[][] result) {
        assertNotNull(result);
        assertEquals(expected.length, result.length);

        for (int[] edge : expected) {
            if (!arrayContains(edge, result)) {
                throw new AssertionError("Array does not contain element: " + Arrays.toString(edge));
            }
        }
    }

    private boolean arrayContains(int[] element, int[][] array) {
        for (int[] cur : array) {
            if (Arrays.equals(element, cur)) {
                return true;
            }
        }

        return false;
    }
}
