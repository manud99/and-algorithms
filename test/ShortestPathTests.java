import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ShortestPathTests {

    int[][][] edges;

    /**
     * <img src="./doc-files/graph-shortest-path.svg">
     */
    @Before
    public void setupTest() {
        this.edges = new int[][][]{
                {{1, 5}, {2, 2}, {3, 3}}, // a
                {{4, 3}}, // b
                {{3, 2}, {5, 8}}, // c
                {{1, 1}, {4, 3}}, // d
                {{5, 3}, {6, 1}}, // e
                {{3, 1}}, // f
                {{5, 1}}, // g
        };
    }

    @Test
    public void testBFS() {
        BFSShortestPath bfs = new BFSShortestPath();
        // We ignore the edges' weights in this example
        int res = bfs.find(edges, 0, 6);

        assertEquals(3, res);
    }

    @Test
    public void testDijkstra() {
        int[] distances = new Dijkstra().find(edges, 0);

        assertArrayEquals(new int[]{0, 4, 2, 3, 6, 8, 7}, distances);
    }

    @Test
    public void testBellmanFord() {
        BellmanFord bellmanFord = new BellmanFord();
        int[] distances = bellmanFord.find(edges, 0);

        assertArrayEquals(new int[]{0, 4, 2, 3, 6, 8, 7}, distances);
    }
}
