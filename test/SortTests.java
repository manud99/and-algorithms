import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SortTests {

    @Test
    public void testBubbleSort() {
        int[] arr = unsortedArray();
        BubbleSort bubbleSort = new BubbleSort();

        bubbleSort.sort(arr);

        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, arr);
    }

    @Test
    public void testSelectionSort() {
        int[] arr = unsortedArray();
        SelectionSort selectionSort = new SelectionSort();

        selectionSort.sort(arr);

        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, arr);
    }

    @Test
    public void testInsertionSort() {
        int[] arr = unsortedArray();
        InsertionSort insertionSort = new InsertionSort();

        insertionSort.sort(arr);

        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, arr);
    }

    @Test
    public void testMergeSort() {
        int[] arr = unsortedArray();
        MergeSort mergeSort = new MergeSort();

        mergeSort.sort(arr);

        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, arr);
    }

    @Test
    public void testHeapSort() {
        int[] arr = unsortedArray();
        HeapSort heapSort = new HeapSort();

        heapSort.sort(arr);

        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, arr);
    }

    @Test
    public void testQuickSort() {
        int[] arr = unsortedArray();
        QuickSort quickSort = new QuickSort();

        quickSort.sort(arr);

        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, arr);
    }

    @Test
    public void testQuickSortWithDuplicates() {
        int[] arr = unsortedArrayWithDuplicates();
        QuickSort quickSort = new QuickSort();

        quickSort.sort(arr);

        assertArrayEquals(new int[]{1, 1, 2, 3, 4, 5, 6, 6, 7, 8, 9, 9}, arr);
    }

    private int[] unsortedArray() {
        return new int[]{9, 4, 6, 2, 8, 7, 1, 5, 3};
    }

    private int[] unsortedArrayWithDuplicates() {
        return new int[]{9, 9, 4, 6, 2, 8, 6, 7, 1, 1, 5, 3};
    }
}
