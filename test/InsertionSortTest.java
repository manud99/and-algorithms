import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InsertionSortTest {

    @Test
    public void testSearchElement() {
        int[] arr = {1, 2, 3, 5, 7, 8, 9, 10};
        InsertionSort insertionSort = new InsertionSort();

        int res = insertionSort.binarySearch(arr, 6, 5);
        assertEquals(3, res);
    }

    @Test
    public void testSearchJustSmallerThanMid() {
        int[] arr = {1, 2, 3, 5, 7, 8, 9, 10};
        InsertionSort insertionSort = new InsertionSort();

        int res = insertionSort.binarySearch(arr, 6, 4);
        assertEquals(3, res);
    }

    @Test
    public void testSearchJustGreaterThanMid() {
        int[] arr = {1, 2, 3, 5, 7, 8, 9, 10};
        InsertionSort insertionSort = new InsertionSort();

        int res = insertionSort.binarySearch(arr, 6, 6);
        assertEquals(4, res);
    }

    @Test
    public void testSearchSmallerThanArray() {
        int[] arr = {1, 2, 3, 5, 7, 8, 9, 10};
        InsertionSort insertionSort = new InsertionSort();

        int res = insertionSort.binarySearch(arr, 6, 0);
        assertEquals(0, res);
    }

    @Test
    public void testSearchGreaterThanArray() {
        int[] arr = {1, 2, 3, 5, 7, 8, 9, 10};
        InsertionSort insertionSort = new InsertionSort();

        int res = insertionSort.binarySearch(arr, 6, 10);
        assertEquals(7, res);
    }
}
