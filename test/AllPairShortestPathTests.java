import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AllPairShortestPathTests {

    private int[][] adjMatrix;

    /**
     * <img src="./doc-files/graph-all-pair-shortest-path.png">
     */
    @Before
    public void setup() {
        // MAX_VALUE means there is no edge, all other values are the edges' weight
        this.adjMatrix = new int[][]{
                {Integer.MAX_VALUE, -3, Integer.MAX_VALUE, 2},
                {5, Integer.MAX_VALUE, 3, Integer.MAX_VALUE},
                {1, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE},
                {-1, Integer.MAX_VALUE, 4, Integer.MAX_VALUE},
        };
    }

    @Test
    public void testFloydWarshall() {
        FloydWarshall floydWarshall = new FloydWarshall();
        int[][] result = floydWarshall.solve(adjMatrix, 4);

        int[][] expected = new int[][]{
                {0, -3, 0, 2},
                {4, 0, 3, 6},
                {1, -2, 0, 3},
                {-1, -4, -1, 0},
        };

        assertNotNull(result);
        assertEquals(expected.length, result.length);
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }
    }

    @Test
    public void testJohnson() {
        Johnson johnson = new Johnson();
        int[][] result = johnson.solve(adjMatrix, 4);

        int[][] expected = new int[][]{
                {0, -3, 0, 2},
                {4, 0, 3, 6},
                {1, -2, 0, 3},
                {-1, -4, -1, 0},
        };

        assertNotNull(result);
        assertEquals(expected.length, result.length);
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], result[i]);
        }
    }
}
