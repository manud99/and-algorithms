import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SearchTests {

    @Test
    public void testBinarySearch() {
        BinarySearch binarySearch = new BinarySearch(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
        int res = binarySearch.search(1);
        assertEquals(1, res);
    }

    @Test
    public void testBinarySearchNotFound() {
        BinarySearch binarySearch = new BinarySearch(new int[]{0, 2, 3, 4, 5, 6, 7, 8, 9});
        int res = binarySearch.search(1);
        assertEquals(-1, res);
    }

    @Test
    public void testBinarySearchNotSorted() {
        BinarySearch binarySearch = new BinarySearch(new int[]{9, 4, 6, 2, 8, 7, 1, 5, 3});
        int res = binarySearch.search(1);
        assertEquals(-1, res);
    }

    @Test
    public void testLinearSearchFindNumber() {
        LinearSearch linearSearch = new LinearSearch(new int[]{9, 4, 6, 2, 8, 7, 1, 5, 3});
        int res = linearSearch.search(5);
        assertEquals(7, res);
    }

    @Test
    public void testLinearSearchNotFound() {
        LinearSearch linearSearch = new LinearSearch(new int[]{9, 4, 6, 2, 8, 7, 1, 3});
        int res = linearSearch.search(5);
        assertEquals(-1, res);
    }
}
