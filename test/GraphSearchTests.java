import org.junit.Test;

import java.util.ArrayList;
import java.util.Set;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class GraphSearchTests {

    @Test
    public void testBFS() {
        Node a = generateGraph().get(0);
        BFS bfs = new BFS();

        Set<Node> markedNodes = bfs.search(a);

        assertEquals(8, markedNodes.size());
    }

    @Test
    public void testDFS() {
        Node a = generateGraph().get(0);
        DFS dfs = new DFS();

        Set<Node> markedNodes = dfs.search(a);

        assertEquals(8, markedNodes.size());
    }

    @Test
    public void testDFSIterative() {
        Node a = generateGraph().get(0);
        DFS dfs = new DFS();

        Set<Node> markedNodes = dfs.searchIterative(a);

        assertEquals(8, markedNodes.size());
    }

    @Test
    public void testTopologicalSorting() {
        ArrayList<Node> graph = generateGraph();
        TopologicalSorting topologicalSorting = new TopologicalSorting();

        // Remove this edge to make the graph acyclic ...
        graph.get(3).adjList.remove(0);

        Node[] result = topologicalSorting.find(graph);
        Node[] expected = new Node[]{graph.get(0), graph.get(2), graph.get(3), graph.get(1), graph.get(4), graph.get(7), graph.get(5), graph.get(6)};

        assertArrayEquals(expected, result);
    }

    /**
     * <img src="./doc-files/graph-search.svg">
     */
    private ArrayList<Node> generateGraph() {
        ArrayList<Node> vertices = new ArrayList<>();

        Node a = new Node();
        Node b = new Node();
        Node c = new Node();
        Node d = new Node();
        Node e = new Node();
        Node f = new Node();
        Node g = new Node();
        Node h = new Node();

        vertices.add(a);
        vertices.add(b);
        vertices.add(c);
        vertices.add(d);
        vertices.add(e);
        vertices.add(f);
        vertices.add(g);
        vertices.add(h);

        a.adjList.add(b);
        a.adjList.add(c);
        a.adjList.add(f);
        b.adjList.add(e);
        c.adjList.add(d);
        d.adjList.add(a);
        d.adjList.add(h);
        e.adjList.add(f);
        e.adjList.add(g);
        e.adjList.add(h);
        f.adjList.add(b);
        f.adjList.add(g);
        h.adjList.add(g);

        return vertices;
    }
}