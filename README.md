# AnD Algorithms

Algorithms that were discussed in the AnD lecture in the fall semester 2021 of ETH Zurich.

## Graphs

Here are the visual representation of the graphs:

### Search Graph

The graph where we can run BFS and DFS starting from node `a`.
Without the edge `(a, d)`, it has a topological sorting: `a-c-d-b-e-h-f-g`.

![Search Graph](./test/doc-files/graph-search.svg)

## Shortest path

These are the shortest paths for the following graph:
`{a: 0, b: 4, c: 2, d: 3, e: 6, f: 8, g: 7}`

![Shortest path](./test/doc-files/graph-shortest-path.svg)

## Spanning tree

![Spanning tree](./test/doc-files/graph-spanning-tree.svg)

## All-pair shortest path

![All-pair shortest path](./test/doc-files/graph-all-pair-shortest-path.png)
